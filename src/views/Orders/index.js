import Axios from 'Boot/Axios'
import Separator from 'Components/Separator'
import OrdersList from 'Modules/OrdersListModule'
import {useAsync} from 'react-async-hook'
import {useHistory} from 'react-router-dom'
import {withErrorBoundary} from 'react-error-boundary'

export default withErrorBoundary(() => {
  const history = useHistory()
  const {error, loading, result} = useAsync(async () => await Axios.get('/orders'), [])

  if (loading) {
    return (
      <div>Loading...</div>
    )
  }

  if (error) {
    throw error
  }

  return (
    <>
      <div css={{
        display: 'flex',
        alignItems: 'center',
        height: 48,
        margin: '48px 0',
        fontSize: 32,
        fontWeight: 'bold'
      }}>
        <div css={{
          display: 'flex',
          alignItems: 'center'
        }}>
          {result.data.data.length}
          <div css={{
            fontSize: '40%',
            lineHeight: 1,
            fontWeight: 'normal',
            margin: 8,
            color: '#BABABA'
          }}>
            <div>orders</div>
            <div>displayed</div>
          </div>
        </div>
        <Separator vertical spaced />
      </div>
      <OrdersList data={result.data.data} onRowClick={({values}) => history.push(`/orders/${values.id}`)}/>
    </>
  )
}, {
  FallbackComponent: () => <div>Something went wrong...</div>
})