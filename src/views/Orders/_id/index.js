import Axios from 'Boot/Axios'
import {Context} from 'Boot/ConstantsContext'
import Separator from 'Components/Separator'
import DetailedOrder from 'Modules/DetailedOrder'
import {useParams} from 'react-router-dom'
import {useAsync} from 'react-async-hook'
import {withErrorBoundary} from 'react-error-boundary'

export default withErrorBoundary(() => {
  const {id} = useParams()
  const {state: {couriers}} = React.useContext(Context)  
  const {error, loading, result} = useAsync(async () => await Axios.get('/orders/:id', {
    params: {
      id: 9
    }
  }), [])

  
  if (loading) {
    return (
      <div>Loading...</div>
    )
  }

  if (error) {
    throw error
  }
  
  return (
    <>
      <div css={{
        display: 'flex',
        alignItems: 'center',
        height: 48,
        margin: '48px 0',
        fontSize: 32,
        fontWeight: 'bold'
      }}>
        <div>{id}</div>
        <Separator vertical spaced />
        <div css={{
          flex: 1
        }}>{result.data.seller}</div>
        <Separator vertical spaced />
        <div>{
          (couriers.find(({id}) => id === result.data.courier) || {}).description ?? 'Unknown courier'
        }</div>
      </div>
      <DetailedOrder data={result.data.events} />
    </>
  )
}, {
  FallbackComponent: () => <div>Something went wrong...</div>
})