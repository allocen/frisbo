import 'Boot/Axios'
import {Provider} from 'Boot/ConstantsContext'

injectGlobal`
  * {
    box-sizing: border-box;
  }
  :root {
    --color-success-h: 145;
    --color-success-s: 80%;
    --color-success-l: 38%;

    --color-error-h: 5;
    --color-error-s: 75%;
    --color-error-l: 42%;

    --color-warning-h: 15;
    --color-warning-s: 88%;
    --color-warning-l: 53%;

    --color-info-h: 202;
    --color-info-s: 86%;
    --color-info-l: 50%;
  }
  html, body, #root {
    width: 100%;
    height: 100%;
  }
  body {
    background-color: #fff;
    color: #5C5C5C;
    fill: #5C5C5C;
    font-family: Roboto, monospace;
    font-size: 14px;
    line-height: 1.48;
    margin: 0;
  }
`

const LazyComponent = React.lazy(async () => {
  await new Promise(resolve => setTimeout(resolve, 800))

  return import('./boot/App')
})
  
ReactDOM.render(
  <React.StrictMode>
    <Provider>
      <React.Suspense fallback={
        <div css={{
          height: '100%',
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
          justifyContent: 'center'
        }}>
          Loading...
        </div>
      }>
        <LazyComponent />
      </React.Suspense>
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
)

// export const TypesContext = React.createContext(InitialState);