import Separator from './Separator'
import { configure, mount, shallow } from 'enzyme'
import Adapter from '@wojtekmaj/enzyme-adapter-react-17'

configure({
  adapter: new Adapter()
})

describe('Component: Separator', () => {
  it('Should render without throwing an error', () => {
    expect(shallow(<Separator />)).not.toBeNull()
  })

  it('`spaced` prop type should be a boolean only', () => {
    const wrapper = mount(<Separator spaced />)

    expect(wrapper.prop('spaced')).toEqual(true)
  })
  it('`vertical` prop type should be a boolean only', () => {
    const wrapper = mount(<Separator vertical />)

    expect(wrapper.prop('vertical')).toEqual(true)
  })
  it('should act like a horizontal line if `vertical` prop not specified', () => {
    expect(getComputedStyle(mount(<Separator />).getDOMNode()).getPropertyValue('width')).toBe('100%');
  })
  it('should act like a vertical line if `vertical` prop is specified', () => {
    expect(getComputedStyle(mount(<Separator vertical />).getDOMNode()).getPropertyValue('width')).toBe('1px');
  })
})