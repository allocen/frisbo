function Component ({spaced, vertical}) {
  return (
    <div css={{
      backgroundColor: '#EAEAEA',
      ...vertical ? {
        width: 1,
        height: '100%',
        ...spaced && {
          marginLeft: 32,
          marginRight: 32
        }
      } : {
        width: '100%',
        height: 1,
        ...spaced && {
          marginTop: 32,
          marginBottom: 32
        }
      }
    }}></div>
  )
}

Component.propTypes = {
  spaced: PropTypes.bool,
  vertical: PropTypes.bool
}

export default Component