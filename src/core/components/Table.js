import {useTable} from 'react-table'

const Component = ({columns, data, onRowClick, getCellProps, getRowProps}) => {
  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    prepareRow
  } = useTable({
    columns,
    data
  })

  return (
    <table css={{
      width: '100%',
      borderSpacing: 0
    }} {...getTableProps()}>
      <thead css={{
        color: '#BABABA'
      }}>
        <tr css={{
          'th': {
            '&:first-of-type, &:last-of-type': {
              textAlign: 'center'
            }
          }
        }} {...headerGroups[1].getHeaderGroupProps()}>
          {headerGroups[1].headers.map(column => (
            <th css={{
              height: 40,
              borderBottom: '2px #F1F1F1 solid'
            }} align="left" width={column.width} {...column.getHeaderProps()}>{column.render('Header')}</th>
          ))}
        </tr>
      </thead>
      <tbody css={{
        color: '#5C5C5C'
      }} {...getTableBodyProps()}>
        {rows.map((row, i) => {
          prepareRow(row)

          return (
            <tr css={{
              backgroundColor: '#fff',
              cursor: 'pointer',
              'td': {
                '&:first-of-type, &:last-of-type': {
                  textAlign: 'center'
                }
              },
              '&:last-of-type td': {
                borderBottom: 0
              },
              '&:hover': {
                backgroundColor: '#F1F1F1'
              }
            }} onClick={() => onRowClick(row)} {...row.getRowProps(getRowProps(row))}>
              {row.cells.map(cell => {
                return <td css={{
                  height: 48,
                  borderBottom: '1px #F1F1F1 solid',
                  '&:first-of-type': {
                    borderTopLeftRadius: 4,
                    borderBottomLeftRadius: 4
                  },
                  '&:last-of-type': {
                    borderTopRightRadius: 4,
                    borderBottomRightRadius: 4
                  }
                }} align="left" {...cell.getCellProps(getCellProps(cell))}>{cell.render('Cell')}</td>
              })}
            </tr>
          )
        })}
      </tbody>
    </table>
  )
}

Component.defaultProps = {
  onRowClick: () => void(0),
  getCellProps: () => void(0),
  getRowProps: () => void(0)
}

Component.propTypes = {
  columns: PropTypes.array.isRequired,
  data: PropTypes.array.isRequired
}

export default  Component