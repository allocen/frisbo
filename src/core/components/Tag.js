function Component ({color, solid, outlined, children}) {
  const colors = [
    'success',
    'error',
    'warning',
    'info'
  ].reduce((a, b) => 
    ((a[b] = [`var(--color-${b}-h)`, `var(--color-${b}-s)`, `var(--color-${b}-l)`]), a), {}
  )

  return (
    <div css={{
      borderRadius: 4,
      padding: '4px 8px',
      fontSize: 11,
      textTransform: 'uppercase',
      fontWeight: 'bold',
      display: 'inline-flex',
      cursor: 'pointer',
      ...color ? {
        ...outlined ? {
          color: `hsl(${colors[color].join(',')})`,
          border: `1px ${colors[color] && `hsl(${colors[color].join(',')})`} solid`,
        } : (solid ? {
          backgroundColor: colors[color] && `hsl(${colors[color].join(',')})`,
          color: '#fff'
        }: {
          backgroundColor: colors[color] && `hsla(${colors[color].join(',')}, .25)`,
          color: `hsl(${colors[color].join(',')})`
        })
      } : {
        border: '1px #CECECE solid',
      }
    }}>{children}</div>
  )
}
  
Component.propTypes = {
  color: PropTypes.string,
  solid: PropTypes.bool,
  outlined: PropTypes.bool
}

export default Component