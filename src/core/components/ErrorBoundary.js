export default class extends React.Component {
  constructor (props) {
    super(props)

    this.state = {
      hasError: false
    }
  }

  static getDerivedStateFromError (error) {
    this.setState(state => ({
      hasError: true
    }))
  }

  componentDidCatch(error, errorInfo) {
    console.error(error)
  }

  render() {
    if (this.state.hasError) {
      return (
        <div>Something went wrong.</div>
      )
    }

    return this.props.children
  }
}