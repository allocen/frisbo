import Axios from 'axios'
import qs from 'qs'
import MockAdapter from 'axios-mock-adapter'
import { configure } from 'axios-hooks'
import Faker from 'faker'

export let PENDING_REQUESTS = 0
export const CANCEL_TOKEN_SOURCES = new Map()

const AxiosInstance = Axios.create({
  baseURL: 'https://localhost:1234',
  paramsSerializer: function (params) {
    return qs.stringify(params, {arrayFormat: 'brackets'})
  },
  timeout: 56000
})
  
AxiosInstance.interceptors.request.use(async config => {
  PENDING_REQUESTS++

  if (!config.hasOwnProperty('cancelToken')) { 
    const source = Axios.CancelToken.source()

    CANCEL_TOKEN_SOURCES.set(source.token, source.cancel)

    config.cancelToken = source.token
  }

  config.url = config.url.replace(/:(\w+)/g, (_, key) => {
    if (key) {
      if (['string', 'number'].includes(typeof key)) {
        if ((config.params || {})[key]) {
          const url = config.params[key]

          delete config.params[key]

          return url
        }
      }

      return ''
    }
  }).replace(/\/$/, '')

  if (config.hasOwnProperty('notify')) {
    if (typeof config.notify === 'object') {
      config.notify = {
        error: true,
        success: true,

        ...config.notify
      }
    }
  } else {
    config.notify = {
      error: true,
      success: true
    }
  }
      
  return config
}, error => {
  return Promise.reject(error)
})
  
AxiosInstance.interceptors.response.use(response => {
  PENDING_REQUESTS--

  if (response.config.cancelToken) {
    CANCEL_TOKEN_SOURCES.delete(response.config.cancelToken)
  }

  if (response.config.notify) {
    if (response.config.notify.success) {
      if (response.config.notify.success.showMessage) {     
        if (response.data.message) {
          // Show toast
        }
      }
    }
  }
  
  return Promise.resolve(response)
}, async error => {
  PENDING_REQUESTS--

  if (Axios.isCancel(error)) {
    CANCEL_TOKEN_SOURCES.delete(error.message)
  }

  if (error.response) {
    if (error.response.status === 401) {
      LocalStorage.remove('token')
      // Remove locally saved token
    }
    
    if (error.config.notify) {
      if (error.config.notify.error) {
        // Show toast
      }
    }
  } else if (error.request) {
    console.error(error.request)
  } else {
    if (typeof error.message !== 'object') {
      // Show toast
    }
  }
  
  return Promise.reject(error)
})

configure({
  axios: AxiosInstance
})

const Mock = new MockAdapter(AxiosInstance, { delayResponse: 800 })

Mock.onGet('/constants').reply(200, {
  data: {
    statuses: [{
      id: 1,
      name: 'generated',
      description: 'AWB Generated'
    },
    {
      id: 2,
      name: 'cancelled',
      description: 'AWB Cancelled'
    },
    {
      id: 3,
      name: 'receivedBySender',
      description: 'Received by sender'
    },
    {
      id: 4,
      name: 'inTransit',
      description: 'In Transit'
    },
    {
      id: 5,
      name: 'redirected',
      description: 'Redirected'
    },
    {
      id: 6,
      name: 'incorrectAddress',
      description: 'Incorrect Address'
    },
    {
      id: 7,
      name: 'personalPickup',
      description: 'Personal Pickup'
    },
    {
      id: 8,
      name: 'returning',
      description: 'Returning to sender'
    },
    {
      id: 9,
      name: 'unsuccessfullyDelivery',
      description: 'Unsuccessfully Delivery'
    },
    {
      id: 10,
      name: 'refused',
      description: 'Refused'
    },
    {
      id: 11,
      name: 'partialDelivered',
      description: 'Partial Delivered'
    },
    {
      id: 12,
      name: 'delivered',
      description: 'Delivered'
    }],
    couriers: [{
      id: 1,
      description: 'Urgent Cargus'
    }, {
      id: 2,
      description: 'DPD' 
    }, {
      id: 3,
      description: 'Fan Courier'
    }, {
      id: 4,
      description: 'GLS'
    }, {
      id: 5,
      description: 'Sameday'
    }]
  }
})

Mock
  .onGet('/orders')
  .replyOnce(200, {
    data: new Array(100).fill({}).reduce((a, b) => {
      a.push({
        id: Faker.random.number(),
        courier: Faker.random.number({
          min: 1,
          max: 5
        }),
        cod: Faker.random.boolean(),
        awb: {
          initial: {
            value: Faker.random.alphaNumeric(17),
            createdAt: Faker.time.recent()
          },
          return: {
            ...Faker.random.boolean() ? {
              value: Faker.random.boolean() && Faker.random.alphaNumeric(17),
              createdAt: Faker.time.recent()
            } : {
              value: null,
              createdAt: null
            }
          }
        },
        seller: Faker.company.companyName(),
        events: new Array(10).fill({}).reduce((a, b) => {
          a.push({
            id: Faker.random.number(),
            name: Faker.lorem.sentence(),
            description: Faker.lorem.paragraph(),
            createdAt: Faker.time.recent(),
            status: Faker.random.number({
              min: 1,
              max: 12
            })
          })

          return a
        }, []),
        createdAt: Faker.time.recent()
      })

      return a
    }, [])
  })
  .onGet('/orders')
  .replyOnce(500)
  .onGet('/orders/9').reply(200, {
    id: 9,
    courier: Faker.random.number({
      min: 1,
      max: 5
    }),
    cod: Faker.random.boolean(),
    awb: {
      initial: {
        value: Faker.random.alphaNumeric(17),
        createdAt: Faker.time.recent()
      },
      return: {
        ...Faker.random.boolean() ? {
          value: Faker.random.boolean() && Faker.random.alphaNumeric(17),
          createdAt: Faker.time.recent()
        } : {
          value: null,
          createdAt: null
        }
      }
    },
    seller: Faker.company.companyName(),
    events: new Array(10).fill({}).reduce((a, b) => {
      a.push({
        id: Faker.random.number(),
        name: Faker.lorem.sentence(),
        description: Faker.lorem.paragraph(),
        createdAt: Faker.time.recent(),
        status: Faker.random.boolean() && Faker.random.number({
          min: 1,
          max: 12
        })
      })
  
      return a
    }, []),
    createdAt: Faker.time.recent()
  })

export default AxiosInstance