import Axios from 'Boot/Axios'
import Router from 'Boot/Router'
import {Context} from 'Boot/ConstantsContext'
import {useAsync} from 'react-async-hook'

export default () => {
  const {update} = React.useContext(Context)
  const {error, loading} = useAsync(async () => {
    const {data} = await Axios.get('/constants')

    update(data.data)
  }, []);

  if (loading) {
    return (
      <div>Loading...</div>
    )
  }

  return (
    <Router />
  )
}