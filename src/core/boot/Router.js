import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom'
import Main from 'Layouts/Main'
import Orders from 'Views/Orders'
import OrderId from 'Views/Orders/_id'

export default () => {
  return (
    <Router>
      <Switch>
        <Route>
          <Main>
            <Switch>
              <Redirect from="/" to="/orders" exact />
              <Route path="/orders" exact component={Orders} />
              <Route path="/orders/:id" exact component={OrderId} />
              <Route component={() => (<div>404 not found</div>)} exact path='/*' />
            </Switch>
          </Main>
        </Route>
      </Switch>
    </Router>
  )
}