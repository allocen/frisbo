const initialState = {}

export const Context = React.createContext(initialState)

export const Provider = ({ children }) => {
  const [state, update] = React.useReducer((state, payload) => ({
    ...state,
    ...payload
  }), initialState)

  return (
    <Context.Provider value={{state, update}}>
      {children}
    </Context.Provider>
  )
}