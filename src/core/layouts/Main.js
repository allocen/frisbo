import Logo from 'Components/Logo'
import Separator from 'Components/Separator'
import {ArrowBackSVGIcon} from '@react-md/material-icons'
import {useLocation, useHistory} from 'react-router-dom'

export default ({children}) => {
  const {pathname} = useLocation()
  const history = useHistory()
  const [showBackButton, setShowBackButton] = React.useState(false)

  React.useEffect(() => {
    if (pathname === '/orders') {
      setShowBackButton(false)
    } else {
      setShowBackButton(true)
    }
  }, [pathname])

  return (
    <div css={{
      width: 1200,
      margin: '0 auto'
    }}>
      <div css={{
        display: 'flex',
        alignItems: 'center',
        height: 64,
        padding: 16,
        borderBottom: '1px #EAEAEA solid'
      }}>
        {showBackButton && <ArrowBackSVGIcon css={{
          width: 32,
          marginRight: 32,
          cursor: 'pointer',
          opacity: .8,
          '&:hover': {
            opacity: 1
          }
        }} onClick={history.goBack} />}
        <Logo color="#EAEAEA" size={80} />
        <Separator spaced vertical />
      </div>
      {children}
    </div>
  )
}