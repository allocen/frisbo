import {Context} from 'Boot/ConstantsContext'
import Table from 'Components/Table'
import Tag from 'Components/Tag'
import {format} from 'date-fns'
import {DoneSVGIcon} from '@react-md/material-icons'

const Module = ({data, onRowClick}) => {
  const {state: {statuses, couriers}} = React.useContext(Context)  
  
  const columns = React.useMemo(() => [{
    Header: 'Info',
    columns: [
      {
        Header: 'Frisbo #',
        accessor: 'id'
      },
      {
        Header: 'Courier',
        accessor: 'courier',
        width: 64,
        Cell: ({value}) => {
          const src = {
            1: require('url:Assets/img/couriers/cargus.png'),
            2: require('url:Assets/img/couriers/dpd.png'),
            3: require('url:Assets/img/couriers/fan.png'),
            4: require('url:Assets/img/couriers/gls.png'),
            5: require('url:Assets/img/couriers/sameday.png')
          }[value]

          if (src) {
            return (
              <img src={src} />
            )
          }

          return (couriers.find(({id}) => id === value) || {}).description ?? 'Unknown courier'
        }
      },
      {
        Header: 'COD',
        accessor: 'cod',
        width: 64,
        Cell: ({value}) => value && <DoneSVGIcon css={{
          width: 24,
          fill: `hsl(var(--color-success-h), var(--color-success-s), var(--color-success-l))`
        }} />
      },
      {
        id: 'initialAwb',
        Header: 'AWB',
        accessor: 'awb',
        Cell: ({value, row}) => {
          const {name} = statuses.find(({id}) => id === row.values.events[row.values.events.length - 1].status)

          return (
            <>
              <div css={{
                fontWeight: 'bold',
                textTransform: 'uppercase'
              }}>{value.initial.value}</div>
              <div css={{
                  ...![
                    'generated',
                    'inTransit',
                    'delivered',
                    'personalPickup',
                    'receivedBySender'
                  ].includes(name) && {
                    color: '#BABABA'
                  }
                }}>{format(value.initial.createdAt, 'dd LLL, yyyy / hh:mm:ss')}
              </div>
            </>
          )
        }
      },
      {
        id: 'returnAwb',
        Header: 'Return AWB',
        accessor: 'awb',
        Cell: ({value, row}) => {
          const {name} = statuses.find(({id}) => id === row.values.events[row.values.events.length - 1].status)

          return (
            value.return.value ? (
              <>
                <div css={{
                  fontWeight: 'bold',
                  textTransform: 'uppercase'
                }}>{value.return.value}</div>
                <div css={{
                  ...![
                    'generated',
                    'inTransit',
                    'delivered',
                    'personalPickup',
                    'receivedBySender'
                  ].includes(name) && {
                    color: '#BABABA'
                  }
                  }}>{format(value.return.createdAt, 'dd LLL, yyyy / hh:mm:ss')}
                </div>
              </> 
            ) : '━━'
          )
        }
      },
      {
        Header: 'Seller',
        accessor: 'seller'
      },
      {
        Header: 'Last Status',
        accessor: 'events',
        width: 192,
        Cell: ({value}) => {
          const {name, description} = statuses.find(({id}) => id === value[value.length - 1].status)
          
          return (
            <Tag solid={[
              'generated',
              'inTransit',
              'delivered',
              'personalPickup',
              'receivedBySender'
            ].includes(name)} color={(() => {
              switch (name) {
                case 'generated':
                case 'inTransit':
                case 'delivered':
                case 'personalPickup':
                  return 'success'
                case 'receivedBySender':
                  return 'info'
                case 'cancelled':
                case 'redirected':
                case 'incorrectAddress':
                case 'returning':
                case 'unsuccessfullyDelivery':
                case 'refused':
                case 'partialDelivered':
                  return 'error'
              }
            })()
            }>{description ?? 'Unknown status'}</Tag>
          )
        }
      },
      {
        Header: 'Timestamp',
        accessor: 'createdAt',
        Cell: ({row, value}) => {
          const {status} = row.values.events[row.values.events.length - 1]
          const {name} = statuses.find(({id}) => id === status)

          return (
            <div css={{
              ...[
                'cancelled',
                'redirected',
                'incorrectAddress',
                'returning',
                'unsuccessfullyDelivery',
                'refused',
                'partialDelivered'
              ].includes(name) && {
                color: `hsl(var(--color-error-h), var(--color-error-s), var(--color-error-l))`,
              }
            }}>
              <div css={{
                fontWeight: 'bold',
              }}>{format(value, 'dd LLL, yyyy')}</div>
              <div>{format(value, 'hh:mm:ss')}</div>
            </div>
          )
        }
      }
    ]
  }], [])

  return (
    <Table
      columns={columns}
      data={data}
      onRowClick={onRowClick}
      getRowProps={({values}) => {
        const {status} = values.events[values.events.length - 1]
        const {name} = statuses.find(({id}) => id === status)

        switch (name) {
          case 'generated':
          case 'inTransit':
          case 'delivered':
          case 'personalPickup':
            return {
              style: {
                color: `hsl(var(--color-success-h), var(--color-success-s), var(--color-success-l))`,
                backgroundColor: `hsla(var(--color-success-h), var(--color-success-s), var(--color-success-l), .25)`
              }
            }
          case 'receivedBySender':
            return {
              style: {
                color: `hsl(var(--color-info-h), var(--color-info-s), var(--color-info-l))`,
                backgroundColor: `hsla(var(--color-info-h), var(--color-info-s), var(--color-info-l), .25)`
              }
            }
          case 'cancelled':
          case 'redirected':
          case 'incorrectAddress':
          case 'returning':
          case 'unsuccessfullyDelivery':
          case 'refused':
          case 'partialDelivered':
            return {
              style: {
                // color: `hsl(var(--color-error-h), var(--color-error-s), var(--color-error-l))`,
              }
            }
        }
      }}
    />
  )
}

Module.defaultProps = {
  onRowClick: () => void(0)
}

Module.propTypes = {
  data: PropTypes.array.isRequired
}

export default Module