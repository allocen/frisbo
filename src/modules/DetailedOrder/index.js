import Loader from './Loader'

const LazyComponent = React.lazy(() => import('./Module'))

export default props => {
  return (
    <React.Suspense fallback={<Loader />}>
      <LazyComponent {...props} />
    </React.Suspense>
  )
}