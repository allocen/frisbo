import Axios from 'Boot/Axios'
import {Context} from 'Boot/ConstantsContext'
import Table from 'Components/Table'
import Tag from 'Components/Tag'
import {format} from 'date-fns'

const Module = ({data}) => {
  const {state: {statuses, couriers}} = React.useContext(Context)  
  
  const columns = React.useMemo(() => [{
    Header: 'Info',
    columns: [
      {
        Header: 'Event ID',
        accessor: 'id',
        width: 96
      },
      {
        Header: 'Event name',
        accessor: 'name'
      },
      {
        Header: 'Event description',
        accessor: 'description'
      },
      {
        Header: 'Event timestamp',
        accessor: 'createdAt',
        width: 128,
        Cell: ({value}) => (
          <>
            <div css={{
              fontWeight: 'bold'
            }}>{format(value, 'dd LLL, yyyy')}</div>
            <div>{format(value, 'hh:mm:ss')}</div>
          </>
        )
      },
      {
        Header: 'Frisbo status',
        accessor: 'status',
        width: 192,
        Cell: ({value}) => {
          if (value) {          
            const {name, description} = statuses.find(({id}) => id === value)
            
            return (
              <Tag color={(() => {
                switch (name) {
                  case 'generated':
                  case 'inTransit':
                  case 'delivered':
                    return 'success'
                  case 'personalPickup':
                  case 'receivedBySender':
                    return 'info'
                  case 'cancelled':
                  case 'redirected':
                  case 'incorrectAddress':
                  case 'returning':
                  case 'unsuccessfullyDelivery':
                  case 'refused':
                  case 'partialDelivered':
                    return 'error'
                }
              })()
              }>{description ?? 'Unknown status'}</Tag>
            )
          }

          return '━━━'
        }
      }
    ]
  }], [])

  return (
    <Table
      columns={columns}
      data={data}
      getRowProps={({values}) => ({
        style: {
          ...values.status && {
            backgroundColor: '#F1F1F1'
          }
        }
      })}
    />
  )
}

Module.propTypes = {
  data: PropTypes.array.isRequired
}

export default Module